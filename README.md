# tsu - toml encoding and decoding utilities,parse to toml

 ## parse string to toml data
 ```rust
{
    let cargo_toml = tsu::toml_from_str(r#"
    [package]
    name = "useful_macro"
    version = "0.2.6"

    [dependencies]
    serde = "1.0"

    [dev-dependencies]
    serde_derive = "1.0"
    serde_json = "1.0"
    "#);
    
    let package = cargo_toml.get("package").unwrap();
    let name = package.get("name").unwrap();
    println!("{:#?}", &package);
    println!("{:#?}", &name);

    let dependencies = cargo_toml.get("dependencies").unwrap();
    println!("{:#?}", &dependencies);

    let dev_dependencies = cargo_toml.get("dev-dependencies").unwrap();
    println!("{:#?}", &dev_dependencies);
}
 ```


 ## convert struct to toml string
 ```
 [package]
name = "demo"
version = "0.1.0"
edition = "2021"

[dependencies]
tsu = "0.1.8"
serde = "1.0.137"
serde_derive = "1.0.137"
 ```
 ```rust
{
    use serde_derive::Serialize;
    #[derive(Serialize)]
    struct Human {
        name: String,
        age: u8,
        country: Country,
    }
    #[derive(Serialize)]
    struct Country {
        name: String,
    }
    let user = Human {
        name: "mike".to_string(),
        age: 18,
        country: Country {
            name: "country_name".to_string(),
        },
    };
    let toml = tsu::to_toml_str(&user);
    std::fs::write("./demo.toml", toml.as_str()).unwrap();
    println!("{}",toml.as_str());
}
 ```


## read toml file and parse contents to toml data
```rust
{
   let cargo_toml = tsu::toml_from_path("./Cargo.toml");
   let dependencies = cargo_toml.get("dependencies").unwrap();
   println!("{:#?}", &dependencies);
}
```

 ## convert_toml_to_json used to convert toml to json
 ```rust
 {
     let toml_data = r#"
             [package]
             name = "package_name"
             version = "0.3.0"
     
             [dependencies]
             serde = "1.0"
     
             [dev-dependencies]
             serde_derive = "1.0"
             serde_json = "1.0"
     "#;
     let json = convert_toml_to_json(toml_data).unwrap();
     assert_eq!("{\n  \"dependencies\": {\n    \"serde\": \"1.0\"\n  },\n  \"dev-dependencies\": {\n    \"serde_derive\": \"1.0\",\n    \"serde_json\": \"1.0\"\n  },\n  \"package\": {\n    \"name\": \"package_name\",\n    \"version\": \"0.3.0\"\n  }\n}",json);
     
 }
 ```

 ## convert_json_to_toml used to convert json to toml
```rust
{
    let json_data=  r#"
    {
        "data":{
            "version":"0.12.0",
            "category":"rust"
        }
    }
    "#;
    let toml = convert_json_to_toml(&json_data).unwrap();
    assert_eq!("[data]\nversion = '0.12.0'\ncategory = 'rust'\n",toml);
}
```


## convert_toml_to_yaml used to convert toml to yaml
 ```rust
let toml_data = r#"
[package]
name = "package_name"
version = "0.3.0"

[dependencies]
serde = "1.0"

[dev-dependencies]
serde_derive = "1.0"
serde_json = "1.0"
"#;
let yaml = convert_toml_to_yaml(toml_data).unwrap();
assert_eq!("dependencies:\n  serde: '1.0'\ndev-dependencies:\n  serde_derive: '1.0'\n  serde_json: '1.0'\npackage:\n  name: package_name\n  version: 0.3.0\n",yaml);
 ```

## convert_yaml_to_toml used to convert yaml to toml
```rust
let yaml_data = r"dependencies:
  serde: '1.0'
dev-dependencies:
  serde_derive: '1.0'
  serde_json: '1.0'
package:
  name: package_name
  version: 0.3.0";
assert_eq!("[dependencies]\nserde = '1.0'\n\n[dev-dependencies]\nserde_derive = '1.0'\nserde_json = '1.0'\n\n[package]\nname = 'package_name'\nversion = '0.3.0'\n",convert_yaml_to_toml(yaml_data).unwrap());
```


