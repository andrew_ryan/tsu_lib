use std::sync::MutexGuard;
use std::sync::Mutex;
use toml::Value;
use serde::ser::Serialize;

/// ## toml_from_str used to parse string to toml data
/// ```ignore
///{
///    let cargo_toml = tsu::toml_from_str(r#"
///    [package]
///    name = "useful_macro"
///    version = "0.2.6"
///
///    [dependencies]
///    serde = "1.0"
///
///    [dev-dependencies]
///    serde_derive = "1.0"
///    serde_json = "1.0"
///    "#);
///    
///    let package = cargo_toml.get("package").unwrap();
///    let name = package.get("name").unwrap();
///    println!("{:#?}", &package);
///    println!("{:#?}", &name);
///
///    let dependencies = cargo_toml.get("dependencies").unwrap();
///    println!("{:#?}", &dependencies);
///
///    let dev_dependencies = cargo_toml.get("dev-dependencies").unwrap();
///    println!("{:#?}", &dev_dependencies);
///}
/// ```
#[allow(warnings)]
#[inline]
pub fn toml_from_str(toml_str:impl ToString)-> MutexGuard<'static, Value> {
    let toml_str_m = Box::leak(Box::new(Mutex::new(toml_str.to_string().parse::<Value>().unwrap())));
    toml_str_m.lock().unwrap()
}

/// ## to_toml_str used to convert struct to toml string
/// ```ignore
///{
///    use serde_derive::Serialize;
///    #[derive(Serialize)]
///    struct Human {
///        name: String,
///        age: u8,
///        country: Country,
///    }
///    #[derive(Serialize)]
///    struct Country {
///        name: String,
///    }
///    let user = Human {
///        name: "mike".to_string(),
///        age: 18,
///        country: Country {
///            name: "country_name".to_string(),
///        },
///    };
///    let toml = tsu::to_toml_str(&user);
///    println!("{}",toml.as_str());
///}
/// ```
#[inline]
pub fn to_toml_str(toml_str:impl Serialize) -> MutexGuard<'static, String> {
    let toml_str_m = Box::leak(Box::new(Mutex::new(toml::to_string(&toml_str).unwrap())));
    toml_str_m.lock().unwrap()
}
/// ## toml_from_path used to read toml file and parse contents to toml data
/// ```ignore
/// {
///    let cargo_toml = tsu::toml_from_path("./Cargo.toml");
///    let dependencies = cargo_toml.get("dependencies").unwrap();
///    println!("{:#?}", &dependencies);
/// }
/// ```
#[inline]
pub fn toml_from_path(toml_path:impl ToString)-> MutexGuard<'static, Value>{
    let toml_content = String::from_utf8_lossy(&std::fs::read(&toml_path.to_string()).unwrap()).to_string();
    toml_from_str(toml_content)
}

/// ## convert_toml_to_json used to convert toml to json
/// ```rust
/// use tsu::*;
/// {
///     let toml_data = r#"
///             [package]
///             name = "package_name"
///             version = "0.3.0"
///     
///             [dependencies]
///             serde = "1.0"
///     
///             [dev-dependencies]
///             serde_derive = "1.0"
///             serde_json = "1.0"
///     "#;
///     let json = convert_toml_to_json(toml_data).unwrap();
///     assert_eq!("{\n  \"dependencies\": {\n    \"serde\": \"1.0\"\n  },\n  \"dev-dependencies\": {\n    \"serde_derive\": \"1.0\",\n    \"serde_json\": \"1.0\"\n  },\n  \"package\": {\n    \"name\": \"package_name\",\n    \"version\": \"0.3.0\"\n  }\n}",json);
///     
/// }
/// ```
#[inline]
pub fn convert_toml_to_json(input:&str) -> Result<String, Box<dyn std::error::Error + 'static>>{
    let val: toml::Value = toml::from_str(input).unwrap();
    let serde_json = serde_json::to_string_pretty(&val).unwrap();
    Ok(serde_json)
}
/// ## convert_json_to_toml used to convert json to toml
/// ```rust
/// use tsu::*;
/// {
///     let json_data=  r#"
///     {
///         "data":{
///             "version":"0.12.0",
///             "category":"rust"
///         }
///     }
///     "#;
///     let toml = convert_json_to_toml(&json_data).unwrap();
///     assert_eq!("[data]\nversion = '0.12.0'\ncategory = 'rust'\n",toml);
/// }
/// ```
#[inline]
pub fn convert_json_to_toml(input:&str) -> Result<String, Box<dyn std::error::Error + 'static>>{
    use serde_json::Deserializer;
    use toml::ser::Serializer;
    let mut toml = String::new();

    let mut deserializer = Deserializer::from_str(input);
    let mut serializer = Serializer::pretty(&mut toml);
    serde_transcode::transcode(&mut deserializer, &mut serializer).unwrap();
    Ok(toml)
}
/// ## convert_toml_to_yaml used to convert toml to yaml
/// ```rust
/// use tsu::*;
///let toml_data = r#"
///[package]
///name = "package_name"
///version = "0.3.0"
///
///[dependencies]
///serde = "1.0"
///
///[dev-dependencies]
///serde_derive = "1.0"
///serde_json = "1.0"
///"#;
///let yaml = convert_toml_to_yaml(toml_data).unwrap();
///assert_eq!("dependencies:\n  serde: '1.0'\ndev-dependencies:\n  serde_derive: '1.0'\n  serde_json: '1.0'\npackage:\n  name: package_name\n  version: 0.3.0\n",yaml);
///```
#[inline]
pub fn convert_toml_to_yaml(input:&str) -> Result<String, Box<dyn std::error::Error + 'static>>{
    let val: toml::Value = toml::from_str(input).unwrap();
    let serde_yaml = serde_yaml::to_string(&val).unwrap();
    Ok(serde_yaml)
}

/// ## convert_yaml_to_toml used to convert yaml to toml
/// ```rust
///use tsu::*;
///let yaml_data = r"dependencies:
///  serde: '1.0'
///dev-dependencies:
///  serde_derive: '1.0'
///  serde_json: '1.0'
///package:
///  name: package_name
///  version: 0.3.0";
///assert_eq!("[dependencies]\nserde = '1.0'\n\n[dev-dependencies]\nserde_derive = '1.0'\nserde_json = '1.0'\n\n[package]\nname = 'package_name'\nversion = '0.3.0'\n",convert_yaml_to_toml(yaml_data).unwrap());
///```
/// 

#[inline]
pub fn convert_yaml_to_toml(input:&str) -> Result<String, Box<dyn std::error::Error + 'static>>{
    use serde_yaml::Deserializer;
    use toml::ser::Serializer;
    let mut toml = String::new();

    let deserializer = Deserializer::from_str(input);
    let mut serializer = Serializer::pretty(&mut toml);
    serde_transcode::transcode(deserializer, &mut serializer).unwrap();
    Ok(toml)
}