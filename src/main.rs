use tsu::*;


#[allow(warnings)]
fn main() {
    {
        let toml_data = r#"
        [package]
        name = "package_name"
        version = "0.3.0"

        [dependencies]
        serde = "1.0"

        [dev-dependencies]
        serde_derive = "1.0"
        serde_json = "1.0"
        "#;

        let cargo_toml = tsu::toml_from_str(toml_data);

        let package = cargo_toml.get("package").unwrap();
        let name = package.get("name").unwrap();
        println!("{:#?}", &package);
        println!("{:#?}", &name);

        let dependencies = cargo_toml.get("dependencies").unwrap();
        println!("{:#?}", &dependencies);

        let dev_dependencies = cargo_toml.get("dev-dependencies").unwrap();
        println!("{:#?}", &dev_dependencies);

        println!("convert:::::::::");
        let json = convert_toml_to_json(toml_data).unwrap();
        let yaml = convert_toml_to_yaml(toml_data).unwrap();
        assert_eq!("{\n  \"dependencies\": {\n    \"serde\": \"1.0\"\n  },\n  \"dev-dependencies\": {\n    \"serde_derive\": \"1.0\",\n    \"serde_json\": \"1.0\"\n  },\n  \"package\": {\n    \"name\": \"package_name\",\n    \"version\": \"0.3.0\"\n  }\n}",json);
        println!("yaml::::\n{}",yaml);
        
  assert_eq!("dependencies:\n  serde: '1.0'\ndev-dependencies:\n  serde_derive: '1.0'\n  serde_json: '1.0'\npackage:\n  name: package_name\n  version: 0.3.0\n",yaml);


let yaml_data = r"dependencies:
  serde: '1.0'
dev-dependencies:
  serde_derive: '1.0'
  serde_json: '1.0'
package:
  name: package_name
  version: 0.3.0";
assert_eq!("[dependencies]\nserde = '1.0'\n\n[dev-dependencies]\nserde_derive = '1.0'\nserde_json = '1.0'\n\n[package]\nname = 'package_name'\nversion = '0.3.0'\n",convert_yaml_to_toml(yaml_data).unwrap());

        let json_data=  r#"
        {
            "data":{
                "version":"0.12.0",
                "category":"rust"
            }
        }
        "#;
        let toml = convert_json_to_toml(&json_data).unwrap();
        assert_eq!("[data]\nversion = '0.12.0'\ncategory = 'rust'\n",toml);

    }

    {
        use serde_derive::Serialize;
        #[derive(Serialize)]
        struct Human {
            name: String,
            age: u8,
            country: Country,
        }
        #[derive(Serialize)]
        struct Country {
            name: String,
        }
        let user = Human {
            name: "mike".to_string(),
            age: 18,
            country: Country {
                name: "country_name".to_string(),
            },
        };
        let toml = tsu::to_toml_str(&user);
        println!("{}",toml.as_str());
    }

    {
        let cargo_toml = tsu::toml_from_path("./Cargo.toml");
        let dependencies = cargo_toml.get("dependencies").unwrap();
        println!("{:#?}", &dependencies);
    }
}